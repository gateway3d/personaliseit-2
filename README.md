Personalise-iT For Magento 2
====

# Introduction

Personalise-iT is a Magento 2 module provided by Gateway3D which allows your Magento installation to integrate into the Gateway3D personalisation platform.

# Installation

Please note that these instructions assume that you have SSH access to your host.

## Getting the Code

### via SSH & Composer

The easiest way to download the module is by using Composer. Simply run the below command in the root folder of your Magento installation.

```
$ composer require gateway3d/magento2-personaliseit2

Using version ^2.0 for gateway3d/magento2-personaliseit2
./composer.json has been updated
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing gateway3d/magento2-personaliseit2 (2.0.0)
    Loading from cache

Writing lock file
Generating autoload files

```

Additional guidance on how to install Composer and how to install Magento using Composer can be found at http://devdocs.magento.com/guides/v2.0/install-gde/prereq/integrator_install.html

## Using Magento's Command Line Tool

Additional information on using Magento2 command line tool can be found at http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands.html

### Enabling the Module

Magento's command line tool can now be used to enable the Personalise-iT module.

From the root Magento directory, run the following.

```
$ bin/magento module:enable Gateway3D_PersonaliseIt

The following modules have been enabled:
- Gateway3D_PersonaliseIt

...
```

### Updating the Database

Subsequently, the Magento command line tool must now also be used to update the database schema.


```
$ bin/magento setup:upgrade

Cache cleared successfully
File system cleanup:

... 

```

# Connecting Magento to OMS

In contrast to the original Personalise-iT module which used a cronjob to push orders into OMS, Personalise-iT 2 now relies on Magento's standard SOAP API to allow OMS to pull orders from the eCommerce site.

To allow this, it is necessary to create a new Magento intregration service which can be accomplished in Magento's admin backend, **System** > **Integration** > **Add New Integration**.

It is recommended that `oms` is entered as the integration name. At a minimum, the new integration should be given access to the `Orders` and `Shipments` APIs.

Once the relevant APIs have been enabled, the integration can be activated after which Magento will display an **Access Token** and an **Access Secret Token**.

Please provide both tokens as well as the integration name to your Gateway3D implementer who will then configure OMS to pull in orders from your site.

# Current Limitations

As Personalise-iT 2 is a complete rewrite, it does not currently have feature parity with the older Magento v1 module. Many features such as dynamic pricing and multiple iframe URLs are not supported but will be added in the near future.

In addition, Personalise-iT 2 is designed to only work with distributed personalisation apps. The URL of a distributed app will look similar to the following:

```
http://g3d-app.co.uk/s/app/acp2/en_GB/default.html
```

This is in contrast to the older format of:

```
http://app.gateway3d.com/acp/app/?l=acp2
```

Please contact support@gateway3d.com if your app does not work via the new URL to discuss upgrade options.






