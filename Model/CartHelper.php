<?php

namespace Gateway3D\PersonaliseIt\Model;

use Magento\Catalog\Model\Product as ProductModel;

class CartHelper extends \Magento\Framework\Model\AbstractModel
{
	protected $_cart;
	protected $_productRepository;
	protected $_optionsApi;

	public function __construct(
		Cart $cart,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Catalog\Api\Data\ProductCustomOptionInterface $options)
	{
		$this->_cart = $cart;
		$this->_productRepository = $productRepository;
		$this->_optionsApi = $options;
	}

	public function processGrouped(array $items, ProductModel $product)
	{
		foreach($items as $item)
		{
			foreach($item->grouped as $child)
			{
				if($child->quantity)
				{
					$product = $this->_productRepository->getById($child->id);

					if(!$product)
					{
						throw new \Exception('Invalid product');
					}

					$_item = clone $item;
					$_item->quantity = $child->quantity;

					$this->processSimple([ $_item ], $product);
				}
			}
		}
	}

	public function processSimple(array $items, ProductModel $product = null)
	{
		foreach($items as $item)
		{
			$_product = $product ?: $this->_productRepository->get($item->sku);

			if(!$_product)
			{
				throw new \Exception('Invalid product');
			}

			$request = new \Magento\Framework\DataObject([
				'qty'		=> $item->quantity,

				'g3d'		=> [ $item ],

				'options'	=> $this->_getOptions($_product)
			]);

			if(isset($item->pricing))
			{
				if(isset($item->extra->state))
				{
					$item->epa = $this->_getEpaUrl($product);

					// This is not a security risk because prices are validated
					// once the order is transfered to OMS.

					$request->setCustomPrice($item->pricing->total / $item->quantity);
				}
				else
				{
					throw new \Exception('Dynamic pricing is only supported in conjunction with save state');
				}
			}

			// Discard what we don't need as the buy request field
			// is limited to 64kb of serialized data and state info
			// quickly fills it up.

			unset($item->extra);

			$this->_processBuyRequest($request, $_product);
		}
	}

	public function processBundle(array $items, ProductModel $product)
	{
		$bundleOptions = [];

		$type = $product->getTypeInstance(true);

		$collection = $type->getSelectionsCollection(
           	    $product->getTypeInstance(true)->getOptionsIds($product),
           	    $product);

		foreach($collection as $option)
		{
			foreach($items as $item)
			{
				if($item->sku == $option['sku'])
				{
					$bundleOptions[$option['option_id']] = $option['selection_id'];

					continue 2;
				}
			}
		}

		$request = new \Magento\Framework\DataObject([
			'qty' => $items[0]->quantity,

			'bundle_option' => $bundleOptions,

			'g3d'	=> $items
		]);
	
		$this->_processBuyRequest($request, $product);
	}

	private function _getOptions(ProductModel $product)
	{
		$attribute = $product->getCustomAttribute('g3d_create_new_cart_item');

		if(!$attribute || $attribute->getValue() == 1)
		{
			$option = $this->_getInternalOption($product);

			return [
				$option->getId() => strtoupper(md5(uniqid("", true))) 
			];
		}
		else
		{
			return [];
		}
	}

	private function _getInternalOption(ProductModel $product)
	{
		$title = "Personalisation Ref";

		foreach($product->getOptions() as $option)
		{
			if($option->getTitle() == $title)
			{
				return $option;
			}
		}

		$option = $this
			->_optionsApi
			->setTitle($title)
			->setType('field')
			->setSortOrder(999)
			->setPrice(0.0)
			->setPriceType('fixed')
			->setIsRequire(false)
			->setMaxCharacters(50)
			->setProductId($product->getId())
			->setStoreId($product->getStoreId());

		$option->save();

		$product->setCanSaveCustomOptions(true);
		$product->addOption($option);

		$product->setData($product->getData() + [ 'has_options' => true ]);
		$product->save();

		return $option;
	}

	private function _getEpaUrl(ProductModel $product)
	{
		$attribute = $product->getCustomAttribute('g3d_app_url_default');

		if($attribute)
		{
			$url = $attribute->getValue();

			if(preg_match("/epa=(.*?)(?:&|$)/", $url, $matches))
			{
				return $matches[1];
			}
		}
		
		return null;
	}

	private function _processBuyRequest($request, ProductModel $product)
	{
		// Update the G3D part of an existing buy request
		// We do this because Cart->addProduct won't do it for us!
		$cartCandidates = $product->getTypeInstance()->prepareForCartAdvanced(
			$request, $product);
	
		if(is_string($cartCandidates))
		{
			throw new \Exception($cartCandidates);
		}
		
		foreach($cartCandidates as $candidate)
		{
			$quoteItem = $this->_cart->getQuote()->getItemByProduct($candidate);

			if($quoteItem)
			{
				$buyRequestOption = $quoteItem->getOptionByCode('info_buyRequest');
				$buyRequest = json_decode($buyRequestOption->getValue(), true);

				if(isset($buyRequest['g3d']))
				{
					$buyRequest['g3d'] = array_merge($buyRequest['g3d'], $request['g3d']);

					$buyRequestOption->setValue(json_encode($buyRequest));
					$quoteItem->saveItemOptions();
				}
			}
		}

		$_product = $this->_productRepository->getById($product->getId(), false, $product->getStoreId(), true);

		// Add product to cart
		$this->_cart->addProduct($_product, $request);
		$this->_cart->save();
	}
}
