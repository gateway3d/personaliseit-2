<?php

namespace Gateway3D\PersonaliseIt\Model\Sales\Order;

use Magento\Sales\Api\Data\OrderItemInterface;

class ItemRepository
	extends \Magento\Sales\Model\Order\ItemRepository
{
	public function save(OrderItemInterface $entity)
	{
		if ($entity->getProductOption()) {
            $request = $this->getBuyRequest($entity);
            $productOptions = $entity->getProductOptions();

			if(isset($productOptions['info_buyRequest']['g3d']))
			{
				$productOptions['info_buyRequest'] = $request->toArray() + [
					'g3d' => $productOptions['info_buyRequest']['g3d']
				];
			}
			else
			{
            	$productOptions['info_buyRequest'] = $request->toArray();
			}

            $entity->setProductOptions($productOptions);
        }

        $this->metadata->getMapper()->save($entity);
        $this->registry[$entity->getEntityId()] = $entity;
        return $this->registry[$entity->getEntityId()];
	}
}

