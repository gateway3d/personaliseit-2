<?php

namespace Gateway3D\PersonaliseIt\Model;

class Cart extends \Magento\Checkout\Model\Cart
{
	/**
	 * Override base class so that we can bypass Magento's filtering of the
	 * custom_price field and set prices explicitly.
	 *
	 * Magento started filtering the custom_price field in 2.1.2
	 *
	 * @see http://devdocs.magento.com/guides/v2.1/release-notes/ReleaseNotes2.1.2CE.html
	 */
	protected function _getProductRequest($requestInfo)
	{
		if($requestInfo instanceof \Magento\Framework\DataObject)
		{
			return $requestInfo;
		}
		else
		{
			throw new \InvalidArgumentException('Unexpected $requestInfo type');
		}
	}
}

