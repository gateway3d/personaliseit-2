<?php

namespace Gateway3D\PersonaliseIt\Model\Api;

use Gateway3D\PersonaliseIt\Api\OrdersInterface;
use Magento\Sales\Api\Data\OrderInterface;

class Orders
	extends \Magento\Sales\Model\OrderRepository
	implements OrdersInterface
{
	public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
	{
		$result = parent::getList($searchCriteria);

		foreach($result->getItems() as $order)
		{
			foreach($order->getItems() as $item)
			{
				$podRef = false;
				$isStock = false;

				$buyRequest = $item->getBuyRequest();

				if($item->getProduct())
				{
					$podRefAttribute = $item->getProduct()->getResource()->getAttribute('g3d_pod_ref');
					$podRef = $podRefAttribute->getFrontend()->getValue($item->getProduct());

					if($isStockAttribute = $item->getProduct()->getCustomAttribute('g3d_is_stock'))
					{
						$isStock = $isStockAttribute->getValue();
					}
				}

				if(isset($buyRequest['g3d']))
				{
					$item->setAdditionalData([
						$buyRequest['g3d']
					]);
				}
				elseif($podRef)
				{
					$item->setAdditionalData([ [ [
						'type'	=> 'pod',
						'ref'	=> $podRef,
						'quantity' => $item->getQtyInvoiced()
					] ] ]);
				}
				elseif($isStock)
				{
					$item->setAdditionalData([ [ [
						'type'	=> 'stock',
						'quantity' => $item->getQtyInvoiced()
					] ] ]);
				}

				if($item->getProduct())
				{
					$textualProductIdAttribute = $item->getProduct()->getResource()->getAttribute('g3d_textual_product_id');
					$variantIdAttribute = $item->getProduct()->getResource()->getAttribute('g3d_variant_id');
					$appUrlAttribute = $item->getProduct()->getResource()->getAttribute('g3d_app_url_default');

					$textualProductId = $textualProductIdAttribute->getFrontend()->getValue($item->getProduct());
					$variantId = $variantIdAttribute->getFrontend()->getValue($item->getProduct());
					$appUrl = $appUrlAttribute->getFrontend()->getValue($item->getProduct());

					if (!$podRef && !$appUrl && $textualProductId && $variantId)
					{
						$item->setAdditionalData([ [ [
							'type' => 'textual',
							'textual_product_id' => $textualProductId,
							'product_variant_id' => $variantId,
							'quantity' => $item->getQtyInvoiced()
						] ] ]);
					}
				}
			}
		}

		return $result;
	}
}
