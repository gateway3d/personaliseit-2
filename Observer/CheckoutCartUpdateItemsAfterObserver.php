<?php

namespace Gateway3D\PersonaliseIt\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

class CheckoutCartUpdateItemsAfterObserver
	implements ObserverInterface
{
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$cart = $observer->getData('cart');
		$info = $observer->getData('info')->getData();

		foreach($info as $itemId => $itemInfo)
		{
            $quoteItem = $cart->getQuote()->getItemById($itemId);

			if($quoteItem)
			{
				$buyRequestOption = $quoteItem->getOptionByCode('info_buyRequest');

				$buyRequest = json_decode($buyRequestOption->getValue(), true);

				if(isset($buyRequest['g3d']))
				{
					if(count($buyRequest['g3d']) == 1)
					{
						$buyRequest['g3d'][0]['quantity'] = $quoteItem->getQty();
					}
					else
					{
                    	throw new LocalizedException(__("Updating quantity is only supported when 'Always Create New Cart Item' is enabled"));
					}

					$buyRequestOption->setValue(json_encode($buyRequest));
					$quoteItem->saveItemOptions();
				}
			}
		}
	}
}

