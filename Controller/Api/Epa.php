<?php

namespace Gateway3D\PersonaliseIt\Controller\Api;

use Magento\Catalog\Model\Product as ProductModel;

class Epa extends \Magento\Framework\App\Action\Action
{
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
		\Magento\Framework\Pricing\Helper\Data $priceHelper,
		\Gateway3D\PersonaliseIt\Controller\Result\JsonpFactory $resultJsonFactory
	)
	{
		parent::__construct($context);
		
		$this->_productRepository = $productRepository;
		$this->_stockRegistry = $stockRegistry;
		$this->_priceHelper = $priceHelper;

		$this->resultJsonFactory = $resultJsonFactory;
	}

	public function execute()
	{
		$result = $this->resultJsonFactory->create();

		$request = $this->getRequest();
		
		$result->setCallback($request->getParam('callback'));

		return $result->setData($this->_getData());
	}

	protected function _getData()
	{
		$id = $this->getRequest()->getParam('id');

		$product = $this->_productRepository->getById($id);

		$data = $this->_getProductData($product);

		$data['grouped'] = [];

		if($product->getTypeId() == \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE)
		{
			$childIds = $product->getTypeInstance()->getChildrenIds($product->getId());
			$childIds = reset($childIds);

			foreach($childIds as $childId)
			{
				$child = $this->_productRepository->getById($childId);

				$data['grouped'][] = $this->_getProductData($child);
			}
		}

		return $data;
	}

	private function _getProductData(ProductModel $product)
	{
		$stockItem = $this->_stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());

		return [
			'id'				=> $product->getId(),
			'sku'				=> $product->getSku(),
			
			'price'				=> $this->_getPrice($product),
				
			'name'				=> $product->getName(),
			'description'		=> $product->getDescription(),
			'rating'			=> null,
			
			'minimum_quantity'	=> $stockItem ? $stockItem->getMinSaleQty() : 0,
			'maximum_quantity'	=> $stockItem ? $stockItem->getMaxSaleQty() : PHP_INT_MAX,
			
			'increment_quantity'	=> $stockItem && $stockItem->getEnableQtyIncrements()
										? $stockItem->getQtyIncrements()
										: 1,
			
			'is_new'			=> false,
			
			'options'			=> [],
			'tiers'				=> [],
		];
	}

	private function _getPrice(ProductModel $product)
	{
		return $this->_priceHelper->currency($product->getFinalPrice(), true, false);
	}
}

