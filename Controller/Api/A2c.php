<?php

namespace Gateway3D\PersonaliseIt\Controller\Api;

use Zend\Json\Json;

use Gateway3D\PersonaliseIt\Model\CartHelper;

use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\Product\Type as ProductType;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;

class A2c
	extends \Magento\Framework\App\Action\Action
	implements CsrfAwareActionInterface
{
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager,
		\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Gateway3D\PersonaliseIt\Model\CartHelper $cartHelper
	)
	{
		parent::__construct($context);

		$this->_resultJsonFactory = $resultJsonFactory;
		$this->_productRepository = $productRepository;
		$this->_sessionManager = $sessionManager;
		$this->_cookieManager = $cookieManager;
		$this->_cookieMetadataFactory = $cookieMetadataFactory;
		$this->_cartHelper = $cartHelper;
	}

	public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
	{
		return null;
	}

	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}

	public function execute()
	{
		$raw = $this->getRequest()->getParam('data');
		$id = $this->getRequest()->getParam('id');
		
		$baseProduct = $this->_productRepository->getById($id);

		$data = Json::decode($this->_fixEncoding($raw));
		
		$result = $this->_resultJsonFactory->create();

		$origin = $this->_getOrigin($baseProduct);

		$this->getResponse()->setHeader('Access-Control-Allow-Origin', $origin);
		$this->getResponse()->setHeader('Access-Control-Allow-Credentials', 'true');

		if($this->getRequest()->isPost())
		{
			try
			{
				if($baseProduct->getTypeId() == ProductType::TYPE_SIMPLE)
				{
					$this->_cartHelper->processSimple($data->items, $baseProduct);
				}
				else if($baseProduct->getTypeId() == ProductType::TYPE_BUNDLE)
				{
					$this->_cartHelper->processBundle($data->items, $baseProduct);
				}
				else if($baseProduct->getTypeId() == \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE)
				{
					$this->_cartHelper->processGrouped($data->items, $baseProduct);
				}
				else
				{
					throw new \Exception('Unsupported product type');
				}

				$this->_invalidateCartCookie();

				$result->setData([
					'redirect' => $this->_url->getDirectUrl('/checkout/cart/index')
				]);
			}
			catch (\Exception $e)
			{
				$result->setHttpResponseCode(400);

				$result->setData([
					'error' => [
						'message' => $e->getMessage()
					]
				]);
			}
		}

		return $result;
	}

	/**
	 * Invalidate the cart part of the section_data_ids cookie so that the
	 * minicart is refreshed and shows the correct values.
	 */
	private function _invalidateCartCookie()
	{
		$cookie = $this->_cookieManager->getCookie('section_data_ids');

		if($cookie)
		{
			$data = \Zend_Json::decode($cookie);

			if(!isset($data['cart']))
			{
				$data['cart'] = date('U');
			}

			$data['cart'] += 1000;
		
			$meta = $this
						->_cookieMetadataFactory
						->createPublicCookieMetadata()
						->setPath('/')
						->setDomain($this->_sessionManager->getCookieDomain());

			$this->_cookieManager->setPublicCookie(
				'section_data_ids',
				\Zend_Json::encode($data),
				$meta
			);
		}
	}

	/**
	 * Encode a string that is valid ISO-8859-1 into UTF-8.
	 *
	 * The verbatim string is returned if the original encoding is not ISO-8859-1
	 */
	private function _fixEncoding($string)
	{
		$detectOrder = [ 'UTF-8', 'ISO-8859-1' ];

		if(mb_detect_encoding($string, $detectOrder, true) == 'ISO-8859-1')
		{
			return utf8_encode($string);
		}
		else
		{
			return $string;
		}
	}

	private function _getOrigin(ProductModel $product)
	{
		$attribute = $product->getCustomAttribute('g3d_app_url_default');

		$url = $attribute ? $attribute->getValue() : false;

		if($url)
		{
			$uri = new \Zend\Uri\Uri($url);

			$requestScheme = $this->getRequest()->isSecure() ? 'https' : 'http';

			return implode(array_filter(array(
				$uri->getScheme() ?: $requestScheme,
				'://',
				$uri->getHost(),
				$uri->getPort()
			)));
		}
		else
		{
			return false;
		}
	}
}
