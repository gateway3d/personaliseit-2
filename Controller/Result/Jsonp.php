<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Gateway3D\PersonaliseIt\Controller\Result;

use Magento\Framework\App\Response\HttpInterface;
use Magento\Framework\Controller\Result\Json;

class Jsonp extends Json
{
	protected $_callback = "";

	public function setCallback($callback)
	{
		$this->_callback = $callback;
	}

	protected function render(HttpInterface $response)
    {
        $this->translateInline->processResponseBody($this->json, true);

		$response->setHeader('content-type', 'text/javascript', true);
		$response->setContent("{$this->_getSanitisedCallback()}({$this->json})");
		
		return $this;
	}

	protected function _getSanitisedCallback()
	{
		return preg_replace("/[^0-9a-zA-Z\$_]/", "", $this->_callback);
	}
}

