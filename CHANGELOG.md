# Change Log

## 2.3.2.0 - 2020-01-29

* Workaround for https://github.com/magento/magento2/issues/22431

## 2.3.1.0 - 2020-01-16

* Smartlink postMessage functionality now works correctly when Magento is running on a non standard port number (e.g. 8080)
* Smartlink postMessage is now used to handle add to cart events. This avoids issues with 3rd party cookie restrictions that will be soon introduced in Chrome 80

## 2.3.0.0 - 2019-04-26

** This release will require you to run `bin/magento setup:upgrade`. **
* Updates for Magento 2.3 compatibility. The minimum Magento version is now 2.3.

## 2.2.2.0 - 2018-11-09

* Added support for EPA URLs
* Added support for grouped products

## 2.2.1.2 - 2018-09-05

* Minor bug fixes

## 2.2.1.1 - 2018-08-02

* Updated the order list to check if an order items product has been deleted and skip it if so

## 2.2.1.0 - 2018-07-19

** This release will require you to run `bin/magento setup:upgrade`. **

* Support for stock products has been added. Please see the "Is Stock" toggle on the product setup page.

## 2.2.0.0 - 2018-05-01

* Updates for Magento 2.2 compatibility. The minimum Magento version is now 2.2.

## 2.1.6.0 - 2018-03-02

** This release will require you to run `bin/magento setup:upgrade`. **

* Support for print on demand products has been added

## 2.1.5.1 - 2017-08-25

** This release will require you to run `bin/magento setup:upgrade`. **

* Use default thumbnail in cart instead of the first occurring.

## 2.1.5.0 - 2017-06-05

** This release will require you to run `bin/magento setup:upgrade`. **

* Added support for specifying a different app URL for smaller screen sizes.

## 2.1.4.2 - 2017-03-22

* Fix minor typo

## 2.1.4.1 - 2017-03-22

** This release may require you to run `bin/magento setup:di:compile`. **

* Invalidate the `cart` section of the `section_data_ids` cookie so that the mini cart is refreshed after adding to cart.

* Fix origin URL when the app URL is specified using a relative scheme.

## 2.1.4.0 - 2017-03-01

* Added `checkout_cart_update_items_after` observer to facilitate updating quantities.

* Unset `product.info.addtocart.additional` and `product.info.options` blocks when a product has a Personalise-iT App URL set.

## 2.1.3.0 - 2017-01-31

* By default, the add to cart callback will now create a new cart item for each item passed to it by a personalisation app. This can be changed on a per product basis.

* Work around for Magento 2.1.2 change which disallows setting the custom_price field on a buy request.

* No longer stores the `state` add to cart callback property in the buy request object. This is because the backing field used by Magento for the buy request object has a max length of 64kb.

* Ensure that any ISO-8859-1 encoded A2C callback data is re-encoded as valid UTF-8. 
