<?php

namespace Gateway3D\PersonaliseIt\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
 
	/**
	 * Init
	 *
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}
 
	/**
	 * {@inheritdoc}
	 */
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		if(!$context->getVersion())
		{
			return;
		}
		else
		{
			if(version_compare($context->getVersion(), '2.1.3.0') < 0)
			{
				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
	
				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_create_new_cart_item',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
							'group' => 'Gateway3D Personalise-It',
							'input' => 'select',
							'label' => 'Always Create New Cart Item',
							'required' => false,
							'sort_order' => 15,
							'used_in_product_listing' => true,
							'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
							'source' => 'Magento\Catalog\Model\Product\Attribute\Source\Boolean',
							'visible_on_front' => true

						]);
			}

			if(version_compare($context->getVersion(), '2.1.5.0') < 0)
			{
				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
	
				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_app_url_mobile',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
							'group' => 'Gateway3D Personalise-It',
							'input' => 'text',
							'label' => 'Mobile App URL',
							'required' => false,
							'sort_order' => 11,
							'type' => 'text',
							'used_in_product_listing' => true,
							'visible_on_front' => true
						]);

			}

			if(version_compare($context->getVersion(), '2.1.6.0') < 0)
			{
				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
	
				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_pod_ref',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
							'group' => 'Gateway3D Personalise-It',
							'input' => 'text',
							'label' => 'Print On Demand Reference',
							'required' => false,
							'sort_order' => 11,
							'type' => 'text',
							'used_in_product_listing' => true,
							'visible_on_front' => true
						]);
			}

			if(version_compare($context->getVersion(), '2.2.1.0') < 0)
			{
				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
	
				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_is_stock',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
							'group' => 'Gateway3D Personalise-It',
							'input' => 'boolean',
							'label' => 'is Stock',
							'default_value' => 0,
							'required' => false,
							'sort_order' => 16,
							'used_in_product_listing' => true,
							'visible_on_front' => false
						]);
			}

			if (version_compare($context->getVersion(), '2.3.2.1') < 0) {
				/** @var EavSetup $eavSetup */
				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);				
	
				$groupId = (int)$eavSetup->getAttributeGroupByCode(
					\Magento\Catalog\Model\Product::ENTITY,
					'Default',
					'gateway3d-personalise-it',
					'attribute_group_id'
				);
				
				$eavSetup->updateAttributeGroup(
					\Magento\Catalog\Model\Product::ENTITY,
					'Default', 
					$groupId,  
					'attribute_group_name', 
					'Custom Gateway Personalise-It'
				);
			}

			if(version_compare($context->getVersion(), '2.3.2.4') < 0)
			{

				$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_textual_product_id',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
							'input' => 'text',
							'label' => 'Textual Product ID (Textual Orders Only)',
							'required' => false,
							'sort_order' => 18,
							'type' => 'text',
							'used_in_product_listing' => true,
							'visible_on_front' => true
					]);

					$id = $eavSetup->getAttributeId(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_textual_product_id'
					);

					$attributeSetId = $eavSetup->getDefaultAttributeSetId(\Magento\Catalog\Model\Product::ENTITY);
					$eavSetup->addAttributeToGroup(\Magento\Catalog\Model\Product::ENTITY, $attributeSetId, 'gateway3d-personalise-it', $id, 18);



				$eavSetup
					->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_variant_id',
						[
							'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
							'input' => 'text',
							'label' => 'Variant ID (Textual Orders Only)',
							'required' => false,
							'sort_order' => 20,
							'type' => 'text',
							'used_in_product_listing' => true,
							'visible_on_front' => true
					]);

					$id = $eavSetup->getAttributeId(
						\Magento\Catalog\Model\Product::ENTITY,
						'g3d_variant_id'
					);

					$attributeSetId = $eavSetup->getDefaultAttributeSetId(\Magento\Catalog\Model\Product::ENTITY);
					$eavSetup->addAttributeToGroup(\Magento\Catalog\Model\Product::ENTITY, $attributeSetId, 'gateway3d-personalise-it', $id, 20);
			}
		}
	}
}
