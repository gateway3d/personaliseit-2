<?php

namespace Gateway3D\PersonaliseIt\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
 
	/**
	 * Init
	 *
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(EavSetupFactory $eavSetupFactory)
	{
		$this->eavSetupFactory = $eavSetupFactory;
	}
 
	/**
	 * {@inheritdoc}
	 */
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		/** @var EavSetup $eavSetup */
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

		$eavSetup
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_create_new_cart_item',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D Personalise-It',
					'input' => 'select',
					'label' => 'Always Create New Cart Item',
					'required' => false,
					'sort_order' => 15,
					'used_in_product_listing' => true,
					'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
					'source' => 'Magento\Catalog\Model\Product\Attribute\Source\Boolean',
					'visible_on_front' => true
				])
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_app_url_default',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D Personalise-It',
					'input' => 'text',
					'label' => 'Default App URL',
					'required' => false,
					'sort_order' => 10,
					'type' => 'text',
					'used_in_product_listing' => true,
					'visible_on_front' => true
				])
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_app_url_mobile',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D Personalise-It',
					'input' => 'text',
					'label' => 'Mobile App URL',
					'required' => false,
					'sort_order' => 11,
					'type' => 'text',
					'used_in_product_listing' => true,
					'visible_on_front' => true
				])
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_pod_ref',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'Gateway3D Personalise-It',
					'input' => 'text',
					'label' => 'Print On Demand Reference',
					'required' => false,
					'sort_order' => 11,
					'type' => 'text',
					'used_in_product_listing' => true,
					'visible_on_front' => true
				])
			->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'g3d_is_stock',
				[
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
					'group' => 'Gateway3D Personalise-It',
					'input' => 'boolean',
					'label' => 'Is Stock',
					'default_value' => 0,
					'required' => false,
					'sort_order' => 16,
					'used_in_product_listing' => true,
					'visible_on_front' => false
				]);
	}
}
